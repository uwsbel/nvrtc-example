#include <iostream>
#include <vector>

#include "GpuModuleNv.h"
#include "GpuKernelNv.hpp"

#include "cudalloc.hpp"

#include <cuda_runtime.h>


int main(int argc, char** argv) {

	int devs;
	cudaGetDeviceCount(&devs);
	if (devs < 1) {
		std::cerr << "ERROR: no CUDA-compatible devices were detected!\n";
		exit(1);
	}
	cudaSetDevice(0);

	// Create a CUDA event in order to force the runtime to initialize the context
	cudaEvent_t startup;
	cudaEventCreate(&startup);

	NVPTXModule saxpy_mod;
	saxpy_mod.load("saxpy.ptx");
	void* handle = saxpy_mod.handle();

	GpuKernelNv<int, float, float*, float*, float*> saxpy(handle, "_Z5saxpyifPKfS0_Pf");

	std::vector<float, cudallocator<float>> a(512, 1), b(512, 1), c(512, 0);

	cudaError_t res = cudaGetLastError();
	if (res != CUDA_SUCCESS) {
		std::cout << "Kernel failed! " << std::to_string(res) << "\n";
	}

	GpuKernelParams parms { {1, 1, 1}, {1, 1, 512}, 0, 0 };
	saxpy.invoke(parms, 512, 1.0f, &a[0], &b[0], &c[0]);
	cudaDeviceSynchronize();

	std::cout << "Result array: \n";
	for (auto val : c) {
		std::cout << val << " ";
	}
	std::cout << std::endl;

	cudaEventDestroy(startup);

	std::cout << "Done.\n";

	return 0;
}
