// Copyright 2021 Colin Vanden Heuvel
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
//
// 1. Redistributions of source code must retain the above copyright notice, 
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright 
// notice, this list of conditions and the following disclaimer in the 
// documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its 
// contributors may be used to endorse or promote products derived from this 
// software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef NV_KERNEL_INTERFACE_H
#define NV_KERNEL_INTERFACE_H

#include <tuple>
#include <functional>
#include <vector>
#include <cuda_runtime.h>

#include "GpuKernel.h"

template <typename... Args>
class GpuKernelNv : public GpuKernel<Args...> {
public:
	// Default (empty) constructor
	GpuKernelNv() : GpuKernel<Args...>() {};
	
	// Parameterized constructor
	GpuKernelNv(void* handle, const std::string& symbol) 
		: GpuKernel<Args...>(handle, symbol) {};

	virtual void invoke(GpuKernelParams kparams, Args&&... kargs) {

		CUfunction kfunc;
		CUresult status;
		if ((status = cuModuleGetFunction(
			&kfunc,
			static_cast<CUmodule>(this->kernel_handle),
			this->kernel_symbol.c_str()
		)) != CUDA_SUCCESS) {
			throw std::runtime_error(std::string("Device entry function lookup failed with error code ") + std::to_string(status));
		}

		dim3 gridDim ;
		std::tie(gridDim.x, gridDim.y, gridDim.z) = std::get<0>(kparams);

		dim3 blockDim;
		std::tie(blockDim.x, blockDim.y, blockDim.z) = std::get<1>(kparams);

		std::vector<void*> packed_args;

		[&packed_args](auto&&... args) {
			(packed_args.push_back(static_cast<void*>(&args)), ...);
		}(std::forward<Args>(kargs)...);
		

		cudaStream_t stream = static_cast<cudaStream_t>(std::get<3>(kparams));

		cudaLaunchCooperativeKernel(
			kfunc,
			gridDim,				// Grid dimensions
			blockDim,				// Block dimensions
			packed_args.data(),		// Packed arguments
			std::get<2>(kparams),	// Shared memory size
			stream					// Stream ID
		);
	}

};


#endif
