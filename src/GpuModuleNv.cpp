// Copyright 2021 Colin Vanden Heuvel
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
//
// 1. Redistributions of source code must retain the above copyright notice, 
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright 
// notice, this list of conditions and the following disclaimer in the 
// documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its 
// contributors may be used to endorse or promote products derived from this 
// software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <iostream>
#include <fstream>
#include <vector>

#include "GpuModuleNv.h"

NVPTXModule::NVPTXModule() : GpuModule(), module_handle(nullptr) {};
NVPTXModule::NVPTXModule(const std::string& path) : GpuModule(path) {};

NVPTXModule::~NVPTXModule() {
	if (this->module_handle != nullptr) {
		cuModuleUnload(this->module_handle);
	}
}

void NVPTXModule::load(const std::string& path) {

	// Open as ate to read the end position right away
	std::ifstream source(path, std::ios_base::ate | std::ios_base::binary);

	if (!source) {
		throw std::runtime_error(std::string("Could not open file ") + path);
	}

	// Get the file size
	auto file_size = source.tellg();
	source.seekg(0);

	// Make enough room to buffer the entire file
	this->ptx.resize(file_size);
	source.read(&this->ptx[0], file_size);

	this->module_handle = this->create_module();
}

void* NVPTXModule::handle() {
	return static_cast<void*>(this->module_handle);
}

CUmodule NVPTXModule::create_module() {

	// TODO: make the return buffer size a programmable option
	const unsigned int BUFFER_SIZE = 8192;
	std::vector<char> errors(BUFFER_SIZE);

	// NOTE: these will be populated from somewhere else later on
	std::vector<CUjit_option> opts {
		CU_JIT_ERROR_LOG_BUFFER, // get a log of any compilation errors
		CU_JIT_ERROR_LOG_BUFFER_SIZE_BYTES, // get the size of the buffer
		CU_JIT_TARGET_FROM_CUCONTEXT // compile for the currently active GPU
	};

	// NOTE: these will be populated from somewhere else later on
	std::vector<void*> optvals = {
		static_cast<void*>(&errors[0]),
		reinterpret_cast<void*>(BUFFER_SIZE),
		nullptr
	};

	CUmodule compiled_handle;

	CUresult status;
	if ((status = cuModuleLoadDataEx(
		&compiled_handle,
		&this->ptx[0],
		opts.size(),
		&opts[0],
		&optvals[0]
	)) != CUDA_SUCCESS) {
		std::cerr << "CUDA ERROR CODE: " << status << "\n";
		std::cerr << "JIT ERROR:\n";
		for (auto i = 0; i < 80; i++) {
			std::cerr << "=";
		}
		std::cerr << "\n" << std::string(errors.begin(), errors.end()) << "\n";
		for (auto i = 0; i < 80; i++) {
			std::cerr << "=";
		}
		std::cerr << "\n";

		return nullptr;
	}

	return compiled_handle;
}

NVRTCModule::NVRTCModule() : NVPTXModule() {};
NVRTCModule::NVRTCModule(const std::string& path) : NVPTXModule(path) {};

void NVRTCModule::load(const std::string& path) {
	// TODO: this
	// ...
}

void NVRTCModule::compile_helper() {
	// TODO: this
	// ...
}
