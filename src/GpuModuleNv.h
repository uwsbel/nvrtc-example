// Copyright 2021 Colin Vanden Heuvel
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
//
// 1. Redistributions of source code must retain the above copyright notice, 
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright 
// notice, this list of conditions and the following disclaimer in the 
// documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its 
// contributors may be used to endorse or promote products derived from this 
// software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef NV_MODULE_INTERFACE_H
#define NV_MODULE_INTERFACE_H

#include "GpuModule.h"

#include <vector>

#include <cuda.h>
#include <nvrtc.h>

class NVPTXModule : public GpuModule {
public:
	NVPTXModule();
	NVPTXModule(const std::string& path);
	NVPTXModule(const NVPTXModule& other);
	NVPTXModule(NVPTXModule&& other);

	// Clean up the module handle and any other bound resources
	~NVPTXModule();

	virtual void load(const std::string& path);

	virtual void* handle();

	// TODO: Add options for link flags
	// ...

protected:
	// helper function to convert loaded ptx into a module
	CUmodule create_module();

	std::vector<char> ptx;
	CUmodule module_handle;
};

class NVRTCModule : public NVPTXModule {
public:
	NVRTCModule();
	NVRTCModule(const std::string& path);
	NVRTCModule(const NVRTCModule& other);
	NVRTCModule(NVRTCModule&& other);

	virtual void load(const std::string& path);

	// TODO: Add options for headers
	// ...

	// TODO: Add options for compile flags
	// ...
	
protected:
	// helper function to build the source code into PTX
	void compile_helper();
};

#endif
