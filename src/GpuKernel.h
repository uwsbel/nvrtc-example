// Copyright 2021 Colin Vanden Heuvel
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
//
// 1. Redistributions of source code must retain the above copyright notice, 
// this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright 
// notice, this list of conditions and the following disclaimer in the 
// documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its 
// contributors may be used to endorse or promote products derived from this 
// software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef GPU_KERNEL_INTERFACE_H
#define GPU_KERNEL_INTERFACE_H

#include <cstddef>
#include <tuple>

using GpuKernelDim3 = std::tuple<unsigned int, unsigned int, unsigned int>;
using GpuKernelParams = std::tuple<GpuKernelDim3, GpuKernelDim3, size_t, void*>;

template <typename... Args>
class GpuKernel {
public:
	// Default (empty) constructor
	GpuKernel() {};

	// Construct from handle and symbol name
	GpuKernel(void* handle, const std::string& symbol) 
		: kernel_handle(handle), kernel_symbol(symbol) {};

	void setHandle(void* handle) {
		this->kernel_handle = handle;
	};

	void setSymbol(const std::string& symbol) {
		this->kernel_symbol = symbol;
	};

	virtual void invoke(GpuKernelParams params, Args&&... args) = 0;

	static auto kernel_args(
		GpuKernelDim3 gridDim = GpuKernelDim3(),
		GpuKernelDim3 blockDim = GpuKernelDim3(),
		size_t size = 0,
		size_t stream = 0
	) {
		return std::make_tuple(gridDim, blockDim, size, stream);
	};
	
protected:
	void* kernel_handle;
	std::string kernel_symbol;
};

#endif
